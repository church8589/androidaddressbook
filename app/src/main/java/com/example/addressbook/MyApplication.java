package com.example.addressbook;

import android.app.Application;

public class MyApplication extends Application {

    private AddressBook adbk = new AddressBook();

    public AddressBook getAddressBook() { return adbk;}

    public void setAddressBook(AddressBook adbk) {this.adbk = adbk;}
}
