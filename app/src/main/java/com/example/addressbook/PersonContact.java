package com.example.addressbook;

import java.util.ArrayList;
/**
 *
 * @author Andrew Wilkerson
 *
 */

public class PersonContact extends BaseContact{
    /**
     * An extension of BaseContact to add more specific fields
     * for personal contacts
     */

    private String dateOfBrith;
    private String description;
    private ArrayList<PersonContact> relatives;

    public PersonContact(int phoneNumber, String name, Location location, ArrayList<Photo> photos, String dateOfBirth, String description, ArrayList<PersonContact> relatives) {
        super(phoneNumber, name, location, photos);
        this.dateOfBrith = dateOfBirth;
        this.description = description;
        this.relatives = relatives;
    }

    public PersonContact(int phoneNumber, String name, String dateOfBirth, String description) {
        super(phoneNumber, name);
        this.dateOfBrith = dateOfBirth;
        this.description = description;
        this.relatives = new ArrayList<PersonContact>();
    }

    public PersonContact(String name, int number){
        super(number, name);
    }

    @Override
    public String toString() {
        String information = "P|" + super.toString() + this.dateOfBrith + "|" + this.description;
        return information;
    }


    public String getDateOfBrith() {
        return dateOfBrith;
    }
    public void setDateOfBrith(String dateOfBrith) {
        this.dateOfBrith = dateOfBrith;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Adding relative individually to someone's relatives ArrayList
     * @param contact The relative that needs to be added
     * @return Returns true if relative was added
     */
    public boolean addRelative(PersonContact contact) {
        boolean added = relatives.add(contact);
        return added;
    }

    /**
     * Removing relative individually from someone's relatives ArrayList
     * @param contact The relative that needs to be removed
     * @return Returns true if relative was removed
     */
    public boolean removeRelative(PersonContact contact) {
        boolean removed = relatives.remove(contact);
        return removed;
    }
    public ArrayList<PersonContact> getRelatives() {
        return relatives;
    }
    public void setRelatives(ArrayList<PersonContact> relatives) {
        this.relatives = relatives;
    }





}
