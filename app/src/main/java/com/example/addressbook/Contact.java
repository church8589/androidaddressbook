package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Contact extends AppCompatActivity {

    TextView tv_Name, tv_PhoneNumber;
    Button btn_editcontact, btn_delete, btn_call, btn_text;

    AddressBook adbk;

    FileAccessService fs;

    int phoneNumber;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        adbk = ((MyApplication) this.getApplication()).getAddressBook();

        tv_Name = findViewById(R.id.tv_Name);
        tv_PhoneNumber = findViewById(R.id.tv_PhoneNumber);
        btn_editcontact = findViewById(R.id.btn_editcontact);
        btn_delete = findViewById(R.id.btn_delete);
        btn_call = findViewById(R.id.tv_call);
        btn_text = findViewById(R.id.tv_text);

        Bundle incomingIntent = getIntent().getExtras();

        if(incomingIntent != null){
            String name = incomingIntent.getString("name");
            phoneNumber = incomingIntent.getInt("phoneNumber");
            int position = incomingIntent.getInt("edited");

            tv_Name.setText("Name: " + name);
            tv_PhoneNumber.setText("Phone Number : " + Integer.toString(phoneNumber));

        }

        btn_editcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseContact bp = adbk.searchContactNumber(phoneNumber);
                Intent i = new Intent(getApplicationContext(), AddPersonalContact.class);

                i.putExtra("name", bp.getName());
                i.putExtra("phoneNumber", bp.getPhoneNumber());

                startActivity(i);
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fs = new FileAccessService(v.getContext());

                BaseContact bp = adbk.searchContactNumber(phoneNumber);
                Intent i = new Intent(getApplicationContext(), MainActivity.class);

                adbk.removeContact(bp);

                fs.saveAllContacts(adbk.returnContacts());

                startActivity(i);
            }
        });

        btn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPhoneNumber(Integer.toString(phoneNumber));
            }
        });

        btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                composeMmsMessage(Integer.toString(phoneNumber), "Do you have a moment to talk?");
            }
        });

    }

    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void composeMmsMessage(String phoneNumber, String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + phoneNumber));  // This ensures only SMS apps respond
        intent.putExtra("sms_body", message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
