package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    Button btn_AddContact, btn_SearchContact, btn_loadContacts, btn_showContacts, btn_email, btn_maps;

    EditText et_input;

    ListView lv_contactlist;

    BaseContactAdapter adapter;

    AddressBook adbk;

    FileAccessService fs;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //fs = new FileAccessService(onCreateView().getContext());
        //fs.saveAllContacts(adbk.returnContacts());

        /**try {
            adbk = new AddressBook(fs.readAllContacts());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
         */

        btn_AddContact = findViewById(R.id.btn_AddContact);
        btn_loadContacts = findViewById(R.id.btn_loadContacts);
        btn_SearchContact = findViewById(R.id.btn_SearchContact);
        btn_showContacts = findViewById(R.id.btn_showContacts);
        btn_email = findViewById(R.id.btn_email);
        btn_maps = findViewById(R.id.btn_maps);

        et_input = findViewById(R.id.et_input);


        adbk = ((MyApplication) this.getApplication()).getAddressBook();

        adapter = new BaseContactAdapter(MainActivity.this, adbk);


        adapter.notifyDataSetChanged();

        btn_AddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), AddPersonalContact.class);
                startActivity(i);
            }
        });

        btn_loadContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fs = new FileAccessService(v.getContext());
                try {
                    adbk.copyIntoAddressBook(fs.readAllContacts());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        btn_showContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DisplayContacts.class);
                startActivity(i);
            }
        });

        btn_SearchContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SearchContact.class);
                startActivity(i);
            }
        });

        btn_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String [] addresses = new String [1];

                addresses[0] = et_input.getText().toString();

                composeEmail(addresses, "Hello from Andrew");
            }
        });

        btn_maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mapsQuery = "geo:0,0?q=" + et_input.getText().toString();
                Uri mapuri = Uri.parse(mapsQuery);
                showMap(mapuri);
            }
        });


    }

    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void showMap(Uri geoLocation) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
