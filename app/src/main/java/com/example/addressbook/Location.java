package com.example.addressbook;

/**
 *
 * @author Andrew Wilkerson
 *
 */

public class Location{
    /**
     * Class used to store address locations for contacts
     */

    /**
     * Initial variables for Location
     */
    private static int uniqueIDGen = 0;
    private int iDNumber;
    private String street;
    private String city;
    private String state;

    public Location(String street, String city, String state) {
        uniqueIDGen++;
        this.iDNumber = uniqueIDGen;
        this.street = street;
        this.city = city;
        this.state = state;
    }

    public Location() {}


    public int getiDNumber() {
        return iDNumber;
    }
    public void setiDNumber(int iDNumber) {
        this.iDNumber = iDNumber;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        String locationString = Integer.toString(this.iDNumber) + " " + this.street + " " + this.city + " " + this.state;
        return locationString;
    }

}
