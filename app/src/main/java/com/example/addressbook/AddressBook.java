package com.example.addressbook;

import java.io.FileNotFoundException;
import java.text.CollationElementIterator;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Andrew Wilkerson
 *
 */
public class AddressBook {
    /**
     * This class is used to store contacts for the application
     */

    /**
     * creating local array to add contacts to
     */
    private ArrayList<BaseContact> contacts;

    public AddressBook() {
        this.contacts = new ArrayList<BaseContact>(100);
        /**FileAccessService fs = new FileAccessService();
        try {
            contacts = fs.readAllContacts();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    public AddressBook(ArrayList<BaseContact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(BaseContact contact) {
        contacts.add(contact);
    }

    public void removeContact(BaseContact contact) {
        contacts.remove(contact);
    }

    /**
     * To get the size of the contacts array
     * @return return int for size
     */
    public int size() {
        return contacts.size();
    }

    /**
     * To get the contact at a certain index
     * @param i The index you want to look at
     * @return the contact at that index
     */
    public BaseContact getIndex(int i) {
        return contacts.get(i);
    }

    /**
     * used for displaying information on contact at the index
     * @param index the index of contact you want
     */
    public void display(int index) {
        System.out.println(contacts.get(index).toString());
    }

    /**
     * For printing the entire array at once
     */
    public void printAddressBook() {
        for (BaseContact contact : contacts) {
            System.out.println(contact.toString());
        }
    }

    /**
     * For searching and printing out all contacts found with matching city
     * @param city City you want to search for
     */
    public void searchContactCity(String city) {
        for(BaseContact contact : contacts) {
            try {
                if (contact.getLocation().getCity().equalsIgnoreCase(city)) {
                    System.out.println(contact.toString());
                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching Street
     * @param street Street you want to search for
     */
    public void searchContactStreet(String street) {
        for(BaseContact contact : contacts) {
            try {
                if (contact.getLocation().getStreet().equalsIgnoreCase(street)) {
                    System.out.println(contact.toString());
                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching State
     * @param state State you want to search for
     */
    public void searchContactState(String state) {
        for(BaseContact contact : contacts) {
            try {
                if (contact.getLocation().getState().equalsIgnoreCase(state)) {
                    System.out.println(contact.toString());
                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching Date of Birth
     * @param dob Date oF Birth you want to search for
     */
    public void searchContactBirthDay(String dob) {
        for(BaseContact contact : contacts) {
            try {
                if (contact instanceof PersonContact) {
                    PersonContact person = (PersonContact) contact;
                    if (person.getDateOfBrith().equalsIgnoreCase(dob)) {
                        System.out.println(contact.toString());
                    }

                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching Opening Hour
     * @param hour Opening Hour you want to search for
     */
    public void searchContactOpeningHours(int hour) {
        for(BaseContact contact : contacts) {
            try {
                if (contact instanceof BusinessContact) {
                    BusinessContact business = (BusinessContact) contact;
                    if (business.getBusinessOpen() == hour) {
                        System.out.println(contact.toString());
                    }

                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching Closing Hour
     * @param hour Closing Hour you want to search for
     */
    public void searchContactClosingHours(int hour) {
        for(BaseContact contact : contacts) {
            try {
                if (contact instanceof BusinessContact) {
                    BusinessContact business = (BusinessContact) contact;
                    if (business.getBusinessClose() == hour) {
                        System.out.println(contact.toString());
                    }

                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * For searching and printing out all contacts found with matching website
     * @param website Website you want to search for
     */
    public void searchContactWebsite(String website) {
        for(BaseContact contact : contacts) {
            try {
                if (contact instanceof BusinessContact) {
                    BusinessContact business = (BusinessContact) contact;
                    if (business.getWebsite().equalsIgnoreCase(website)) {
                        System.out.println(contact.toString());
                    }

                }
            }
            catch(NullPointerException e){
                continue;
            }
        }
    }

    /**
     * for searching contacts for a name, and printing to console
     * @param name name you want to search for
     */
    public void searchContactName(String name) {
        for (BaseContact contact : contacts) {
            if (name == contact.getName()) {
                System.out.println(contact.toString());
            }
        }
    }

    /**
     * for searching for a number in contacts
     * @param number number you want to search for
     */
    public BaseContact searchContactNumber(int number) {
        for (BaseContact contact : contacts) {
            if (number == contact.getPhoneNumber()) {
                return contact;
            }
        }
        return null;
    }


    /**
     * Getting the index of a contact in the array
     * @param contact Contact you want to find in the array
     * @return the index of contact in array or -1 if not found
     */
    public int returnContactIndex(BaseContact contact) {
        int index = -1;
        for (BaseContact contact1 : contacts) {
            if (contact == contact1) {
                index = contacts.indexOf(contact);
            }
        }
        return index;
    }

    public void copyIntoAddressBook(ArrayList<BaseContact> arrayList){
        contacts = (ArrayList<BaseContact>)arrayList.clone();
    }

    protected ArrayList<BaseContact> returnContacts(){
        return this.contacts;
    }

}