package com.example.addressbook;

import java.util.ArrayList;
/**
 *
 * @author Andrew Wilkerson
 *
 */
public abstract class BaseContact {
    /**
     * This class is used to build out basic contacts, and extended
     * for more specific contacts
     */

    /**
     * creating local varaibles for BaseContact
     */
    private int phoneNumber;
    private String name;
    private ArrayList<Photo> photos;
    private Location location;

    public BaseContact() {
    }

    public BaseContact(int phoneNumber, String name, Location location, ArrayList<Photo> photos) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        this.location = location;
        this.photos = photos;
    }

    public BaseContact(int phoneNumber, String name) {
        this.phoneNumber = phoneNumber;
        this.name = name;
        Location loc = new Location();
        this.location = loc;
        this.photos = new ArrayList<Photo>();
    }


    public int getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Adds photo to photos
     * @param photo Photo to be added to photos
     * @return Returns true if photo was added to photos
     */
    public boolean addPhoto(Photo photo) {
        boolean added = photos.add(photo);
        return added;
    }

    /**
     * Removes photo from photos
     * @param photo Photo to be removed from photos
     * @return Returns true if photo was removed
     */
    public boolean removePhoto(Photo photo) {
        boolean removed = photos.remove(photo);
        return removed;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }
    public void setPhotos(ArrayList<Photo> photos) {
        this.photos = photos;
    }
    public Location getLocation() {
        return location;
    }
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Overriding toString() to be more specific to BaseContact
     */
    @Override
    public String toString() {
        /**
         String photoString = "";
         for(Photo photo : this.photos) {
         photoString += photo.toString();
         }
         */
        String baseContact = Integer.toString(this.phoneNumber) + "|" + this.name + "|" + this.location.getStreet() + "|" + this.location.getCity() + "|" + this.location.getState() + "|" ;
        return baseContact;
    }



}