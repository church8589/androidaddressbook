package com.example.addressbook;

/**
 *
 * @author Andrew Wilkerson
 *
 */
public class Photo{
    /**
     * Class used to store photo information for BaseContacts and subclasses
     */

    private int iDNumber;
    private String filename;
    private String dateOfPhoto;
    private String description;

    public Photo() {};

    public Photo(int iDNumber, String filename, String dateOfPhoto, String description) {
        this.iDNumber = iDNumber;
        this.filename = filename;
        this.dateOfPhoto = dateOfPhoto;
        this.description = description;
    }

    public int getiDNumber() {
        return iDNumber;
    }
    public void setiDNumber(int iDNumber) {
        this.iDNumber = iDNumber;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public String getDateOfPhoto() {
        return dateOfPhoto;
    }
    public void setDateOfPhoto(String dateOfPhoto) {
        this.dateOfPhoto = dateOfPhoto;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String photo = Integer.toString(this.iDNumber) + " " + this.filename + " " + this.dateOfPhoto + " " + this.description;
        return photo;
    }


}

