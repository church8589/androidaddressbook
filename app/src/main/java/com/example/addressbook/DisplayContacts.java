package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class DisplayContacts extends AppCompatActivity {

    ListView lv_contactlist;

    BaseContactAdapter adapter;

    AddressBook adbk;

    FileAccessService fs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_contacts);


        lv_contactlist = findViewById(R.id.lv_contactlist);

        adbk = ((MyApplication) this.getApplication()).getAddressBook();


        adapter = new BaseContactAdapter(DisplayContacts.this, adbk);

        lv_contactlist.setAdapter(adapter);

        lv_contactlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent i = new Intent(getApplicationContext(), Contact.class);

            BaseContact bp = adbk.returnContacts().get(position);

            i.putExtra("edited", position);
            i.putExtra("name", bp.getName());
            i.putExtra("phoneNumber", bp.getPhoneNumber());

            startActivity(i);
            }
        });
    }
}
