package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Person;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddPersonalContact extends AppCompatActivity {

    EditText et_name, et_phoneNumber;

    Button btn_done;

    AddressBook adbk;

    FileAccessService fs;

    int positionToEdit = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_personal_contact);

        adbk = ((MyApplication) this.getApplication()).getAddressBook();


        et_name = findViewById(R.id.et_name);
        et_phoneNumber = findViewById(R.id.et_phoneNumber);
        btn_done = findViewById(R.id.btn_done);

        Bundle incomingIntent = getIntent().getExtras();

        if(incomingIntent != null){
            String name = incomingIntent.getString("name");
            int phoneNumber = incomingIntent.getInt("phoneNumber");

            et_name.setText(name);
            et_phoneNumber.setText(Integer.toString(phoneNumber));

        }

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fs = new FileAccessService(v.getContext());

                String newName = et_name.getText().toString();
                int newPhoneNumber = Integer.parseInt(et_phoneNumber.getText().toString());

                PersonContact p = new PersonContact(newName, newPhoneNumber);

                BaseContact bp = adbk.searchContactNumber(newPhoneNumber);

                if(bp == null){
                    adbk.addContact(p);
                }
                else{
                    bp.setName(newName);
                    bp.setPhoneNumber(newPhoneNumber);
                }

                //adbk.addContact(p);

                fs.saveAllContacts(adbk.returnContacts());

                Intent i = new Intent(v.getContext(), MainActivity.class);
                startActivity(i);


            }
        });
    }
}
