package com.example.addressbook;

import java.util.ArrayList;
/**
 *
 * @author Andrew Wilkerson
 *
 */
public class BusinessContact extends BaseContact{
    /**
     * An extension of BaseContact to add more specific fields
     * for business contacts
     */

    /**
     * Initial variables specific for BusinessContact
     */
    private int businessOpen;
    private int businessClose;
    private String website;

    public BusinessContact() {};

    public BusinessContact(int phoneNumber, String name, Location location, ArrayList<Photo> photos, int businessOpen, int businessClose, String website) {
        super(phoneNumber, name, location, photos);
        this.businessOpen = businessOpen;
        this.businessClose = businessClose;
        this.website = website;
    }

    public BusinessContact(int phoneNumber, String name, int businessOpen, int businessClose, String website) {
        super(phoneNumber, name);
        this.businessOpen = businessOpen;
        this.businessClose = businessClose;
        this.website = website;
    }

    public int getBusinessOpen() {
        return businessOpen;
    }

    public void setBusinessOpen(int businessOpen) {
        this.businessOpen = businessOpen;
    }

    public int getBusinessClose() {
        return businessClose;
    }

    public void setBusinessClose(int businessClose) {
        this.businessClose = businessClose;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        String information = "B|" + super.toString() + this.businessOpen + "|" + this.businessClose + "|" + this.website;
        return information;
    }
}