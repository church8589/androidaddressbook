package com.example.addressbook;

import android.content.Context;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class FileAccessService implements DataAccessService{

    Context context;

    public FileAccessService(Context context) {this.context = context;}

    public FileAccessService() { }

    @Override
    public ArrayList<BaseContact> readAllContacts() throws FileNotFoundException {

        File path = context.getExternalFilesDir(null);
        File file = new File(path,"addressBook.txt");
        Scanner s = new Scanner(file);

        ArrayList<BaseContact> contacts = new ArrayList<>();

        while(s.hasNextLine()) {
            String line = s.nextLine();

            String [] items = line.split("\\|");

            if(items[0].equalsIgnoreCase("p")) {
                ArrayList<Photo> photos = new ArrayList<>();
                ArrayList<PersonContact> relatives = new ArrayList<>();

                int phoneNumber = Integer.parseInt(items[1]);
                String name = items[2];
                String street = items[3];
                String city = items[4];
                String state = items[5];
                String dob = items[6];
                String description = items[7];
                Location loc = new Location(street, city, state);

                PersonContact contact = new PersonContact(phoneNumber, name, loc, photos, dob, description, relatives);
                contacts.add(contact);
            }

            else if(items[0].equalsIgnoreCase("b")) {
                ArrayList<Photo> photos = new ArrayList<>();

                int phoneNumber = Integer.parseInt(items[1]);
                String name = items[2];
                String street = items[3];
                String city = items[4];
                String state = items[5];
                int openingHours = Integer.parseInt(items[6]);
                int closingHours = Integer.parseInt(items[7]);
                String website = items[8];
                Location loc = new Location(street, city, state);

                BusinessContact contact = new BusinessContact(phoneNumber, name, loc, photos, openingHours, closingHours, website);
                contacts.add(contact);

            }
        }
        return contacts;

    }

    @Override
    public void saveAllContacts(ArrayList<BaseContact> contacts) {
        File path = context.getExternalFilesDir(null);


        try {
            File f = new File(path,"addressBook.txt");
            FileWriter fw = new FileWriter(f);
            PrintWriter pw= new PrintWriter(fw);

            for(BaseContact contact : contacts) {
                String text = contact.toString();
                pw.println(text);

            }
            pw.close();
        }
        catch(IOException e) {
            System.out.println("Error: saveToFile");
        }
    }

}


