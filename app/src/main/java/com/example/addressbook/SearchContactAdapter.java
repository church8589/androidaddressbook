package com.example.addressbook;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class SearchContactAdapter extends BaseAdapter {

    Activity mActivity;
    BaseContact bp;

    public SearchContactAdapter(Activity mActivity, BaseContact bp){
        this.mActivity = mActivity;
        this.bp = bp;
    }
    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View onePersonLine;
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        onePersonLine = inflater.inflate(R.layout.contact_one_line, parent, false);

        TextView tv_name = onePersonLine.findViewById(R.id.tv_name);
        TextView tv_age = onePersonLine.findViewById(R.id.tv_agevalue);
        ImageView iv_icon = onePersonLine.findViewById((R.id.iv_icon));

        tv_name.setText(bp.getName());
        tv_age.setText(Integer.toString(bp.getPhoneNumber()));

        Random r = new Random(9);
        int i = r.nextInt(9 - 1 + 1) + 1;

        int icon_resource_numbers [] = {
                R.drawable.icon01_01,
                R.drawable.icon01_02,
                R.drawable.icon01_03,
                R.drawable.icon01_04,
                R.drawable.icon01_05,
                R.drawable.icon01_06,
                R.drawable.icon01_07,
                R.drawable.icon01_08,
                R.drawable.icon01_09
        };

        iv_icon.setImageResource(R.drawable.icon01_01);

        return onePersonLine;
    }
}
