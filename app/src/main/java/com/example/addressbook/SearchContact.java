package com.example.addressbook;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SearchContact extends AppCompatActivity {

    TextView et_searchPhoneNumber, et_name;
    RadioGroup radioGroup;
    Button btn_search;

    ListView lv_search;

    ArrayAdapter ad;

    AddressBook adbk;

    SearchContactAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);

        btn_search = findViewById(R.id.btn_search);
        et_searchPhoneNumber = findViewById(R.id.et_searchPhoneNumber);
        et_name = findViewById(R.id.et_name);
        lv_search = findViewById(R.id.lv_search);

        adbk = ((MyApplication) this.getApplication()).getAddressBook();

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseContact bp = adbk.searchContactNumber(Integer.parseInt(et_searchPhoneNumber.getText().toString()));

                if(bp != null){
                    adapter = new SearchContactAdapter(SearchContact.this, bp);
                    lv_search.setAdapter(adapter);
                }
                else{
                    adapter.notifyDataSetChanged();

                }





            }
        });
    }
}
